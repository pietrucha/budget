import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { BudgetSummary } from '../../entities/budgetSummary';
import { CashflowSummary } from '../../entities/cashflowSummary';

@Component({
  selector: 'summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SummaryComponent implements OnInit {

  @Input()
  budgetSummary: BudgetSummary;

  @Input()
  cashflowSummary: CashflowSummary;

  ngOnInit() {}

}
