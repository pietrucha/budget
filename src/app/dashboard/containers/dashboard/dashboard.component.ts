import { DashboardService } from './../../dashboard.service';
import { Observable } from 'rxjs';
import { Budget } from '../../entities/budget';
import { CashflowSummary } from '../../entities/cashflowSummary';
import { BudgetSummary } from '../../entities/budgetSummary';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  budgetSummary: BudgetSummary;
  cashflowSummary: CashflowSummary;
  budgets: Budget[];
  budgetsSub: Subscription;
  budgetsLoaded = false;

  constructor(private dashboard: DashboardService) {}

  ngOnInit() {
    this.budgetSummary = new BudgetSummary();
    this.cashflowSummary = new CashflowSummary();
    this.budgetsSub = this.dashboard.getBudgets(null).subscribe(budgets => {
      this.budgets = budgets;
      this.budgetsLoaded = true;
    });
  }

  ngOnDestroy() {
    this.budgetsSub.unsubscribe();
  }

}
