import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Budget } from '../entities/budget';
import { Period } from '../../shared/entities/period';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class BudgetApi {

  constructor(private http: HttpClient) {}

  getBudgets(period?: Period): Observable<Budget[]> {
    return this.http.get('/api/budgets')
      .pipe(
        map((budgets: any[]) => budgets.map(budget => Budget.build(budget)))
      );
  }

}
