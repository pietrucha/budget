import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { Period } from '../shared/entities/period';
import { Budget } from './entities/budget';
import { BudgetApi } from './async/budget.api';
import { Injectable } from '@angular/core';

@Injectable()
export class DashboardService {

  private budgets: Observable<Budget[]>;

  constructor(private budgetApi: BudgetApi) { }

  getBudgets(period?: Period): Observable<Budget[]> {
    if (!this.budgets) {
      this.budgets = this.budgetApi
        .getBudgets(period)
        .pipe(shareReplay(1));
    }
    return this.budgets;
  }
}
