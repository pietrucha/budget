import { RecordsService } from './../../records.service';
import { Component, ElementRef, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { fromEvent, Subscription } from 'rxjs';
import { debounceTime, map, distinctUntilChanged, tap, switchMap } from 'rxjs/operators';

@Component({
  selector: 'records',
  templateUrl: './records.component.html',
  styleUrls: ['./records.component.scss']
})
export class RecordsComponent implements AfterViewInit, OnDestroy {

  @ViewChild('filter', { static: true }) filter: ElementRef;
  dataSource = new MatTableDataSource();
  isLoading = true;
  displayedColumns = ['datetime', 'counterparty', 'category', 'value'];
  keyupSubscription: Subscription;

  constructor(private recordsService: RecordsService) {
    this.recordsService.getRecords()
      .subscribe(data => {
        this.isLoading = false;
        this.dataSource.data = data;
      });
  }

  ngAfterViewInit() {
    this.keyupSubscription = fromEvent(this.filter.nativeElement, 'keyup')
      .pipe(
        debounceTime(1000),
        map((event: Event) => (<HTMLInputElement>event.target).value),
        distinctUntilChanged(),
        tap(() => this.isLoading = true),
        switchMap(value => this.recordsService.searchRecords(value)),
      )
      .subscribe((data) => {
        this.isLoading = false;
        this.dataSource.data = data;
      });
  }

  ngOnDestroy() {
    this.keyupSubscription.unsubscribe();
  }

}
