import { RecordApi } from './async/record.api';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Period } from '../shared/entities/period';
import { Record } from './entities/record';

@Injectable()
export class RecordsService {

  private records: Observable<Record[]>;

  constructor(private recordApi: RecordApi) { }

  getRecords(period?: Period): Observable<Record[]> {
    if (!this.records) {
      this.records = this.recordApi
        .getRecords(period)
        .pipe(shareReplay(1));
    }
    return this.records;
  }

  searchRecords(search: string): Observable<Record[]> {
    return this.recordApi.searchRecords(search);
  }
}
