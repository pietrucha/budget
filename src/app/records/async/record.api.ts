import { Record } from '../entities/record';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Period } from '../../shared/entities/period';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class RecordApi {

  constructor(private http: HttpClient) {}

  getRecords(period: Period): Observable<Record[]> {
    return this.http.get<Record[]>('/api/records');
  }

  searchRecords(search: string): Observable<Record[]> {
    return this.http.get<Record[]>(`/api/records/?counterparty=^${search}`);
  }

}
