import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Record } from '../../entities/record';

@Component({
  selector: 'records-table',
  templateUrl: './records-table.component.html',
  styleUrls: ['./records-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecordsTableComponent {

  @Input()
  dataSource: DataSource<Record[]>;

  @Input()
  displayedColumns: string[];

}
