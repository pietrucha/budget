import { RecordsService } from './records.service';
import { RecordApi } from './async/record.api';
import { NgModule } from '@angular/core';
import { SharedModule } from './../shared/shared.module';
import { RecordsRoutingModule } from './records-routing.module';
import { RecordsComponent } from './containers/records/records.component';
import { MatTableModule } from '@angular/material/table';
import { RecordsTableComponent } from './components/records-table/records-table.component';

@NgModule({
  imports: [
    SharedModule,
    RecordsRoutingModule,
    MatTableModule
  ],
  declarations: [RecordsComponent, RecordsTableComponent],
  providers: [RecordsService, RecordApi]
})
export class RecordsModule { }
