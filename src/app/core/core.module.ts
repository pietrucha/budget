import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localePl from '@angular/common/locales/pl';

import { SharedModule } from './../shared/shared.module';
import { LayoutComponent } from './layout/layout.component';
import { MenuItemDirective } from './menu-item.directive';
import { MatProgressBarModule } from '@angular/material/progress-bar';

registerLocaleData(localePl);

@NgModule({
  imports: [
    BrowserAnimationsModule,
    MatProgressBarModule,
    SharedModule,
    RouterModule
  ],
  declarations: [
    LayoutComponent,
    MenuItemDirective
  ],
  exports: [ LayoutComponent ]
})
export class CoreModule { }
