import { Subscription } from 'rxjs';
import { MenuItemDirective } from './../menu-item.directive';
import { Component, ViewChildren, QueryList, OnDestroy } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Router, NavigationEnd, NavigationStart, NavigationError, NavigationCancel } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnDestroy {

  @ViewChildren(MenuItemDirective)
  private buttons: QueryList<MenuItemDirective>;
  private routerSub: Subscription;
  private selected: string;
  loadingRoute = false;

  constructor(location: Location, router: Router, iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    iconRegistry.addSvgIcon('home', sanitizer.bypassSecurityTrustResourceUrl('assets/home.svg'));
    iconRegistry.addSvgIcon('list', sanitizer.bypassSecurityTrustResourceUrl('assets/list.svg'));
    iconRegistry.addSvgIcon('settings', sanitizer.bypassSecurityTrustResourceUrl('assets/settings.svg'));

    this.routerSub = router.events.subscribe(e => {
      if (e instanceof NavigationStart) {
        this.loadingRoute = true;
      } else if (e instanceof NavigationEnd || e instanceof NavigationError || e instanceof NavigationCancel) {
        this.select(location.path().slice(1));
        this.loadingRoute = false;
      }
    });
  }

  select(name: string) {
    this.selected = name;
    this.buttons.forEach(button => button.isSelected = button.name === name);
  }

  ngOnDestroy() {
    this.routerSub.unsubscribe();
  }

}
