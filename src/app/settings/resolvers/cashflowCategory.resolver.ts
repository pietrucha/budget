import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { CashflowCategory } from '../../shared/entities/cashflowCategory';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { SettingsFacade } from '../settings.facade';

@Injectable()
export class CashflowCategoryResolver implements Resolve<CashflowCategory[]> {

  constructor(private settingsFacade: SettingsFacade) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<CashflowCategory[]> {
    return this.settingsFacade.loadCashflowCategories();
  }

}
