import { Observable } from 'rxjs';
import { SettingsFacade } from './../../settings.facade';
import { CashflowCategory } from '../../../shared/entities/cashflowCategory';
import { Component, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CategoriesComponent } from '../categories/categories.component';

@Component({
  selector: 'settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SettingsComponent {

  forms: FormGroup[] = [];
  cashflowCategories$: Observable<CashflowCategory[]>;

  @ViewChild(CategoriesComponent, { static: true })
  categoriesComponent: CategoriesComponent;

  constructor(private settingsFacade: SettingsFacade) {
    this.cashflowCategories$ = settingsFacade.getCashflowCategories$();
  }

  isAnyFormDirty() {
    return this.categoriesComponent.isAnyFormDirty();
  }

}
