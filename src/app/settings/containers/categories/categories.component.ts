import { Observable } from 'rxjs';
import { SettingsFacade } from './../../settings.facade';
import { CashflowCategory } from '../../../shared/entities/cashflowCategory';
import { Component, Input, ChangeDetectionStrategy, ViewChild, ViewChildren } from '@angular/core';
import { CategoryFormComponent } from '../../components/category-form/category-form.component';
import { CategoryListComponent } from '../../components/category-list/category-list.component';

@Component({
  selector: 'categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoriesComponent {

  @Input()
  cashflowCategories$: CashflowCategory[];
  newCategory: CashflowCategory = new CashflowCategory();
  isUpdating$: Observable<boolean>;

  @ViewChild(CategoryFormComponent, { static: true })
  categoryForm: CategoryFormComponent;

  @ViewChild(CategoryListComponent, { static: true })
  categoryList: CategoryListComponent;

  constructor(private settingsFacade: SettingsFacade) {
    this.isUpdating$ = settingsFacade.isUpdating$();
  }

  addCategory(category: CashflowCategory) {
    this.settingsFacade.addCashflowCategory(category);
  }

  updateCategory(category: CashflowCategory) {
    this.settingsFacade.updateCashflowCategory(category);
  }

  isAnyFormDirty() {
    return this.categoryForm.isDirty() || this.categoryList.isAnyFormDirty();
  }

}
