import { CashflowCategory } from '../../../shared/entities/cashflowCategory';
import { Component, Input, ChangeDetectionStrategy, EventEmitter, Output, ViewChildren, QueryList } from '@angular/core';
import { CategoryFormComponent } from '../category-form/category-form.component';

@Component({
  selector: 'category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CategoryListComponent {

  @Input()
  categories$: CashflowCategory[];

  @Output()
  categoryUpdated: EventEmitter<CashflowCategory> = new EventEmitter();

  @ViewChildren(CategoryFormComponent)
  categoryForms: QueryList<CategoryFormComponent>;

  updateCategory(category: CashflowCategory) {
    this.categoryUpdated.emit(category);
  }

  isAnyFormDirty() {
    return this.categoryForms.reduce((reduced, form) => form.isDirty() || reduced, false);
  }

}
