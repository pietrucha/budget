import { cashflowCategories } from './cashflowCategory.mock';
import { CashflowCategory } from '../shared/entities/cashflowCategory';

function getRandomDate(): Date {
  const date = new Date();
  date.setMonth(9);
  date.setDate(Math.random() * 30 + 1);
  return date;
}

function getRandomCategory() {
  const category = cashflowCategories[Math.floor(Math.random() * cashflowCategories.length)];
  const coutnerparty = category.counterpartyPatterns[0].value;
  return {
    category: category.name,
    counterparty: coutnerparty.charAt(0).toUpperCase() + coutnerparty.slice(1)
  };
}

function getRandomValue() {
  return Math.random() * 300;
}

function generateRecords() {
  const randomRecords = [];
  for (let i = 0; i < 50; i++) {
    randomRecords.push(
      {
        id: i,
        value: getRandomValue(),
        datetime: getRandomDate(),
        ...getRandomCategory()
      }
    );
  }
  return randomRecords.sort((a, b) => a.datetime - b.datetime);
}

export const records = generateRecords();
